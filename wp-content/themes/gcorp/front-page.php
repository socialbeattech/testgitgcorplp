<?php
/**
 * Template Name: Front
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package xseed

 */

get_header();
while ( have_posts() ) : the_post();
?>

<section class="pro" id="pro">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 pt-30 pb-30">
				<h2 class="fs-30 color-gold pt-30 pb-30 text-center">Project Overview</h2>
				<div class="text-justify lato-medium fs-20 fs-xs-16 fs-sm-16 pb-40 pb-xs-10">
					<?php the_field('main-content');?>
				</div>
				<div class="col-sm-12 d-none d-xs-none d-sm-none d-md-block d-lg-block">
					<div class="col-md-4">
						<div class="col-sm-4 ovimg wow zoomIn" data-wow-delay="0.5s"><img src="<?php echo get_template_directory_uri(); ?>/images/overview1.png" alt="Overview" /></div>
						<div class="col-sm-8 fs-sm-13 ovtxt lato-medium pt-30">
							<div class="row">2, 3 & 4 BHK <br> Residential Apartments</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg wow zoomIn" data-wow-delay="0.5s"><img src="<?php echo get_template_directory_uri(); ?>/images/overview2.png" alt="Overview" /></div>
						<div class="col-sm-8 fs-sm-13 ovtxt lato-medium pt-30">
							<div class="row">234 Units <br>(954 sq ft - 2197 sq ft)</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg wow zoomIn" data-wow-delay="0.5s"><img src="<?php echo get_template_directory_uri(); ?>/images/overview3.png" alt="Overview" /></div>
						<div class="col-sm-8 fs-sm-13 ovtxt lato-medium pt-30">
							<div class="row">Located In The <br> Heart Of Koramangala</div>
						</div>
					</div>
					<div class="clr pt-30"></div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg wow zoomIn" data-wow-delay="0.5s"><img src="<?php echo get_template_directory_uri(); ?>/images/overview4.png" alt="Overview" /></div>
						<div class="col-sm-8 fs-sm-13 ovtxt lato-medium pt-30">
							<div class="row">Possession By <br> 2021</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg wow zoomIn" data-wow-delay="0.5s"><img src="<?php echo get_template_directory_uri(); ?>/images/overview5.png" alt="Overview" /></div>
						<div class="col-sm-8 fs-sm-13 ovtxt lato-medium pt-30">
							<div class="row">IGBC Pre-Certified <br> For Gold Rating </div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg wow zoomIn" data-wow-delay="0.5s"><img src="<?php echo get_template_directory_uri(); ?>/images/overview6.png" alt="Overview" /></div>
						<div class="col-sm-8 fs-sm-13 ovtxt lato-medium pt-30">
							<div class="row">20+ <br> Amenities</div>
						</div>
					</div>
				</div>
				<div id="over-mob" class="carousel slide d-block d-xs-block d-sm-block d-md-none d-lg-none" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="col-sm-12 ovimg"><img src="<?php echo get_template_directory_uri(); ?>/images/overview1.png" alt="Overview" /></div>
							<div class="col-sm-12 ovtxt lato-medium pt-xs-10">2, 3 & 4 BHK <br> Residential Apartments</div>

						</div>
						<div class="carousel-item">
							<div class="col-sm-12 ovimg"><img src="<?php echo get_template_directory_uri(); ?>/images/overview2.png" alt="Overview" /></div>
							<div class="col-sm-12 ovtxt lato-medium pt-xs-10">234 Units <br>(954 sq ft - 2197 sq ft)</div>
						</div>
						<div class="carousel-item">
							<div class="col-sm-12 ovimg"><img src="<?php echo get_template_directory_uri(); ?>/images/overview3.png" alt="Overview" /></div>
							<div class="col-sm-12 ovtxt lato-medium pt-xs-10">Located In The <br> Heart Of Koramangala</div>
						</div>
						<div class="carousel-item">
							<div class="col-sm-12 ovimg"><img src="<?php echo get_template_directory_uri(); ?>/images/overview4.png" alt="Overview" /></div>
							<div class="col-sm-12 ovtxt lato-medium pt-xs-10">Possession By <br> 2021</div>
						</div>
						<div class="carousel-item">
							<div class="col-sm-12 ovimg"><img src="<?php echo get_template_directory_uri(); ?>/images/overview5.png" alt="Overview" /></div>
							<div class="col-sm-12 ovtxt lato-medium pt-xs-10">IGBC Pre-Certified <br> For Gold Rating </div>
						</div>
						<div class="carousel-item">
							<div class="col-sm-12 ovimg"><img src="<?php echo get_template_directory_uri(); ?>/images/overview6.png" alt="Overview" /></div>
							<div class="col-sm-12 ovtxt lato-medium pt-xs-10">20+ <br> Amenities</div>
						</div>
					</div>
					<a class="carousel-control-prev" href="#over-mob" data-slide="prev">
						<span class="carousel-control-prev-icon"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
					</a>
					<a class="carousel-control-next" href="#over-mob" data-slide="next">
						<span class="carousel-control-next-icon"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="location" id="loca">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 pt-30 pt-xs-0">
			<div class="col-sm-12 d-block d-xs-block d-sm-none d-md-none d-lg-none"><div class="row">
					<a href="https://www.google.com/maps/place/G+Corp+Residences/@12.9303569,77.6344086,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae1466dd6e22ef:0xd70dcbebfc4472ca!8m2!3d12.9303569!4d77.6365973" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/map-mob.jpg" alt="" class="imgloca mb-20"></a>
					</div></div>
				<div class="row">
					<div class="col-sm-8">
						<div class="row d-none d-xs-none d-sm-block d-md-block d-lg-block">
							<a href="https://www.google.com/maps/place/G+Corp+Residences/@12.9303569,77.6344086,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae1466dd6e22ef:0xd70dcbebfc4472ca!8m2!3d12.9303569!4d77.6365973" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/locationimg.png" alt="" class="imgloca" /></a></div>
					</div>
										
					
					<div class="col-sm-4">
						<div class="row">
							<div class="bg-black pt-30 pb-30">
								<?php the_field('location-text'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="amen" id="ament">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="fs-30 color-gold pt-30 pb-10 text-center">Amenities</h2>
						<h5 class="fs-17 pt-20 text-center lato-medium pb-10">AT GROUND LEVEL</h5>
						<div class="col-sm-12">
							<?php
				                $i=0;
	                          if( have_rows('amenties-ground') ):
	                        while ( have_rows('amenties-ground') ) : the_row();
	                            	if($i % 6 == 0){ ?>
							<div class="col-lg-15 pb-20 pb-xs-0">
								<?php }else{  ?>
								<div class="col-lg-15 pb-20 pb-xs-0">
									<?php }?>
									<img class="pb-xs-20 text-center wow zoomIn" data-wow-delay="0.6s" style="width: 100%;" src="<?php the_sub_field('amenities-img1'); ?>">
									<h5 class="fs-16 text-center pt-10 lato-medium">
										<?php the_sub_field('amenities-txt1'); ?>
									</h5>
								</div>
								<?php $i++; endwhile;
						else :
		            endif;
			      	?>
							</div>
						</div>
						<div class="clr"></div>
						<h5 class="fs-17 pt-30 lato-medium hth pb-10">AT 8TH LEVEL</h5>
						<div class="col-sm-12">
							<?php
				$i=0;
	            if( have_rows('amenties-8level') ):
	                while ( have_rows('amenties-8level') ) : the_row();
	                	if($i % 6 == 0){ ?>
							<div class="col-lg-15 pb-20 pb-xs-0">
								<?php }else{  ?>
								<div class="col-lg-15 pb-20 pb-xs-0">
									<?php }?>
									<img class="pb-xs-20 text-center wow zoomIn" data-wow-delay="0.6s" style="width: 100%;" src="<?php the_sub_field('amenities-img2'); ?>">
									<h5 class="fs-16 text-center pt-10 lato-medium">
										<?php the_sub_field('amenities-txt2'); ?>
									</h5>
								</div>
								<?php $i++; endwhile;
						else :
		            endif;
			      	?>
							</div>
						</div>
						<div class="clr"></div>
						<h5 class="fs-17 pt-30 text-center lato-medium pb-10">AT 14TH LEVEL</h5>
						<div class="col-sm-12 mb-30">
							<?php
				$i=0;
	            if( have_rows('amenties-14level') ):
	                while ( have_rows('amenties-14level') ) : the_row();
	                	if($i % 6 == 0){ ?>
							<div class="offset-1 col-lg-15 pb-40 pb-xs-0">
								<?php }else{  ?>
								<div class="col-lg-15 pb-40 pb-xs-0">
									<?php }?>
									<img class="pb-xs-20 text-center wow zoomIn" data-wow-delay="0.6s" style="width: 100%;" src="<?php the_sub_field('amenities-img'); ?>">
									<h5 class="fs-16 text-center pt-10 lato-medium">
										<?php the_sub_field('amenities-txt'); ?>
									</h5>
								</div>
								<?php $i++; endwhile;
						else :
		            endif;
			      	?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>

</section>
<section class="gall">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="fs-30 color-gold pt-30 pb-0 text-center">Gallery</h2>
			</div>
		  <div class="col-lg-6 col-md-12 col-sm-12 text-center ml-auto mr-auto" id="gallery-actions">
				<ul class="nav nav-tabs text-center ml-auto mr-auto" id="gallery-tab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Floor Plans </a> <span class="lnsp">|</span>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="priject-images-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Projects Images </a> <span class="lnsp2">|</span>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="amenities-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Amenities</a>
					</li>
				</ul>
		  </div>
		</div>
	</div>

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
		<div class="container">
			<div class="row">
				<div class="col-md-10 text-center ml-auto mr-auto">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<?php if (have_rows('gallery-floor')): while ( have_rows('gallery-floor') ) : the_row(); ?>
								<div class="swiper-slide">
									<img src="<?php the_sub_field('galleryfloor-img'); ?>" class="entity-img" />
								</div>
							<?php endwhile; endif; ?>
						</div>
						<div class="swiper-pagination"></div>
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
					</div>
				</div>
			</div>
		</div>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
		<div class="container">
		  <div class="row">
		    <div class="col-md-10 text-center ml-auto mr-auto">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<?php if (have_rows('gallery-floor')): while ( have_rows('gallery-project') ) : the_row(); ?>
								<div class="swiper-slide">
									<img src="<?php the_sub_field('galleryproject-img'); ?>" class="entity-img" />
								</div>
							<?php endwhile; endif; ?>
						</div>
						<div class="swiper-pagination"></div>
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
					</div>
		    </div>
		  </div>
		</div>
  </div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
		<div class="container">
		  <div class="row">
		    <div class="col-md-10 text-center ml-auto mr-auto">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<?php if (have_rows('gallery-floor')): while ( have_rows('gallery-amenities') ) : the_row(); ?>
								<div class="swiper-slide">
									<img src="<?php the_sub_field('galleryamenities-img'); ?>" class="entity-img" />
								</div>
							<?php endwhile; endif; ?>
						</div>
						<div class="swiper-pagination"></div>
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
					</div>
		    </div>
		  </div>
		</div>
  </div>
</div>
</section>
<section class="aboutus" id="about">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-sm-12">
						<div class="row">
							<div class="col-sm-12 d-block d-xs-block d-sm-block d-md-none d-lg-none">
								<div class="col-sm-12">
									<h2 class="fs-28 color-gold pt-30 pb-0 text-center">ABOUT US</h2>
									<img src="<?php echo get_template_directory_uri(); ?>/images/aboutG.png" alt="" class="abG pt-10">
								</div>
							</div>
							<div id="aboutdem" class="carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ul class="carousel-indicators">
									<li data-target="#aboutdem" data-slide-to="0" class="active"></li>
									<li data-target="#aboutdem" data-slide-to="1"></li>
									<li data-target="#aboutdem" data-slide-to="2"></li>
									<li data-target="#aboutdem" data-slide-to="3"></li>

								</ul>

								<!-- The slideshow -->
								<div class="carousel-inner">
									<div class="carousel-item active">
										<img src="<?php echo get_template_directory_uri(); ?>/images/abgal3.png" alt="">
									</div>
									<div class="carousel-item">
										<img src="<?php echo get_template_directory_uri(); ?>/images/abgal2.png" alt="">
									</div>
									<div class="carousel-item">
										<img src="<?php echo get_template_directory_uri(); ?>/images/abgal1.png" alt="">
									</div>
									<div class="carousel-item">
										<img src="<?php echo get_template_directory_uri(); ?>/images/abgal4.png" alt="">
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-lg-5 col-md-5">
						<div class="row">
							<div class="col-sm-12 d-none d-xs-none d-sm-none d-md-block d-lg-block">
								<div class="col-sm-12">
									<h2 class="fs-28 color-gold pt-30 pt-sm-10 pb-0 text-center">ABOUT US</h2>
								</div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/aboutG.png" alt="" class="abG pt-10 pt-sm-0">
							</div>
							<div class="col-sm-12 fs-16 fs-sm-13 aboutxt text-center pt-10 pt-xs-30 lato-medium">
								<?php the_field('about-text'); ?>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>
	</div>
</section>

<?php
endwhile;
get_footer();
