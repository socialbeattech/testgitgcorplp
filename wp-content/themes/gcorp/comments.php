<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Medicord
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

    <?php 
    $default = 'http://www.shriramproperties.com/first-time-home-buyers-guide-india/wp-content/uploads/2018/08/Default-Avatar-Settings-1.jpg';
$comment_author_email = strtolower($comment_author_email);
$size = "48";
$comment_author_emailHash = md5($comment_author_email);
//convert default avatar URL to urlencoded
$default = urlencode($default);
?>
<h2 class="pb-20 pb-xs-10 pb-sm-10">Thoughts on “The Complete Guide for FIRST-TIME Home Buyers in India”</h2>
<?php if($comments) : ?>  
    <ol class="comments">  
    <?php foreach($comments as $comment) : ?>  
        <li id="comment-<?php comment_ID(); ?>" class="list-unstyled <?php if ($comment->user_id == 1) echo "authcomment";?>">  
            <?php if ($comment->comment_approved == '0') : ?>  
                <p>Your comment is awaiting approval</p>  
            <?php endif; ?>             
            <h5> <?php echo "<img src='//gravatar.com/avatar/$comment_author_emailHash?s=$size'>";?>  <?php comment_author_link(); ?></h5>
            <cite> on <small><?php comment_date(); ?></small></cite><br />
            <?php comment_text(); ?>  
        </li>  
    <?php endforeach; ?>  
    </ol>  
<?php endif; ?> 

<?php if(comments_open()) : ?>
	<h2>Leave Your Comment</h2>
	<p>Your email address will not be published. Required fields are marked *</p>
    <?php if(get_option('comment_registration') && !$user_ID) : ?>  
        <p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> to post a comment.</p><?php else : ?>  
        <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">  
            <?php if($user_ID) : ?>  
                <p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Log out &raquo;</a></p>  
            <?php else : ?>  
                <p class="comments-form-pillar"><label for="author">Name* <?php if($req) echo ""; ?></label>  
				<input type="text" pattern="^[a-zA-Z][a-zA-Z\\s]+$" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" /><br/></p>
                <p class="comments-form-pillar"><label for="email">Email* <?php if($req) echo " "; ?></label>
				<input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" /> 
                </p>  
            <?php endif; ?>
            <p class="comments-form-pillar-comment"><label for="email">Comments* <?php if($req) echo " "; ?></label>
			<textarea name="comment" id="comment" class="form-control" rows="5"  tabindex="4"></textarea></p>  
            <?php //show_subscription_checkbox(); ?>
            <p><input name="submit" type="submit" id="submit" tabindex="5" class="btn btn-primary" value="Submit Comment" />  
            <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" /></p>  
            <?php do_action('comment_form', $post->ID); ?>  
        </form>  
    <?php endif; ?>  
<?php else : ?>  
    <p>The comments are closed.</p>  
<?php endif; ?>

</div><!-- #comments -->
