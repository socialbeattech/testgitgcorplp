<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gcorp

 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="amphtml" href="http://residences.gcorpgroup.com/amp"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" />
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NWJ6F7G');</script>
<!-- End Google Tag Manager -->
	<?php wp_head(); ?>
</head>
<body style="overflow-x:hidden;" <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NWJ6F7G"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<header id="masthead" class="site-header">
		<div class="container">
			<div class="row">
				<div class="site-branding col-lg-2 col-md-2 col-sm-12 col-xs-12 text-xs-center text-sm-center text-left">
					<img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" class="site-logo" alt="logo" />
				</div>
				<div class="site-branding col-lg-7 col-md-7 col-sm-12 col-xs-12 text-xs-center text-sm-center text-center fs-18 fs-sm-15 pt-20 pt-sm-10 pt-xs-0">
					<a href="#pro" class="scroll">Project Overview</a> | <a href="#loca" class="scroll">Location</a> | <a href="#ament" class="scroll">Amenities</a> | <a href="#about" class="scroll">About Us</a>
				</div>
				<div class="site-branding col-lg-3 col-md-3 col-sm-12 col-xs-12 fs-18 fs-sm-14  pt-20 pt-sm-10 text-right">
					<div class="col-12 pt-sm-0"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/phone.png" alt="" />7676131888</a></div>


				</div>
			</div>
		</div>
	</header>
	<section class="slider d-none d-xs-none d-sm-none d-md-block d-lg-block">
	<div id="bannerslider" class="carousel slide" data-ride="carousel">
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
	 <img src="<?php echo get_template_directory_uri(); ?>/images/banner.png" alt="" style="width:100%;" />

    </div>
    <div class="carousel-item">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bannerone.png" alt="" style="width:100%;" />
    </div>
  </div>
  </div>
	</section>
	<section class="slider d-block d-xs-block d-sm-block d-md-none d-lg-none">
	<div id="bannerSwiper-mob" class="carousel slide" data-ride="carousel">
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
	 <img src="<?php echo get_template_directory_uri(); ?>/images/banner-mob.png" alt="" style="width:100%;" />

    </div>
    <div class="carousel-item">
   <img src="<?php echo get_template_directory_uri(); ?>/images/bannerone-mob.png" alt="" style="width:100%;" />
    </div>
  </div>
  </div>
	</section>

	<section class="form-styling" id="one">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if(is_front_page()) { ?>
					<div class="col-12 col-sm-12 offset-lg-8 col-lg-4 col-md-12 text-right">
						<div class="form-bg" id="form">
							<h3 class="text-center color-white regular-pro" style="font-weight:normal;">CONTACT US</h3>
							<div class="g-form"><?php the_field('g-form');?></div>
						</div>
					</div>
					<?php } else { ?>
						<div class="col-12 col-sm-12 offset-lg-8 col-lg-4 col-md-12 col-sm-12 text-center mt-100 mt-xs-0 mt-sm-50 mb-sm-50">
							<div class="form-bg">
								<h4 class="color-black regular-pro pt-30 pb-30 color-white">Thanks for contacting with us, will get back to you shortly! </h4>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
