<?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>  
<?php 
/* 
Template Name: gcorp-amp
 */
 ?>
<!DOCTYPE HTML>
<html amp>
<head>
<meta charset="utf-8">
<link rel="canonical" href="http://residences.gcorpgroup.com/" />
<title>Gcorp Residences</title>
<meta name="description" content="">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png"/>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-font" src="https://cdn.ampproject.org/v0/amp-font-0.1.js"></script>
<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
<script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
<!-- AMP Analytics --><script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
<style amp-custom>
*,::before,::after{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:rgba(0,0,0,0)}article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}[tabindex="-1"]:focus{outline:0}hr{box-sizing:content-box;height:0;overflow:visible}h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}abbr[title],abbr[data-original-title]{text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted;cursor:help;border-bottom:0;-webkit-text-decoration-skip-ink:none;text-decoration-skip-ink:none}address{margin-bottom:1rem;font-style:normal;line-height:inherit}ol,ul,dl{margin-top:0;margin-bottom:1rem}ol ol,ul ul,ol ul,ul ol{margin-bottom:0}dt{font-weight:700}dd{margin-bottom:.5rem;margin-left:0}blockquote{margin:0 0 1rem}b,strong{font-weight:bolder}small{font-size:80%}sub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}a{color:#007bff;text-decoration:none;background-color:transparent}a:hover{color:#0056b3;text-decoration:underline}a:not([href]):not([tabindex]){color:inherit;text-decoration:none}a:not([href]):not([tabindex]):hover,a:not([href]):not([tabindex]):focus{color:inherit;text-decoration:none}a:not([href]):not([tabindex]):focus{outline:0}pre,code,kbd,samp{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;font-size:1em}pre{margin-top:0;margin-bottom:1rem;overflow:auto}figure{margin:0 0 1rem}img{vertical-align:middle;border-style:none;max-width:100%;}svg{overflow:hidden;vertical-align:middle}table{border-collapse:collapse}caption{padding-top:.75rem;padding-bottom:.75rem;color:#6c757d;text-align:left;caption-side:bottom}th{text-align:inherit}label{display:inline-block;margin-bottom:.5rem}button{border-radius:0}button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}input,button,select,optgroup,textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button,select{text-transform:none}select{word-wrap:normal}button,[type="button"],[type="reset"],[type="submit"]{-webkit-appearance:button}button:not(:disabled),[type="button"]:not(:disabled),[type="reset"]:not(:disabled),[type="submit"]:not(:disabled){cursor:pointer}button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner{padding:0;border-style:none}input[type="radio"],input[type="checkbox"]{box-sizing:border-box;padding:0}input[type="date"],input[type="time"],input[type="datetime-local"],input[type="month"]{-webkit-appearance:listbox}textarea{overflow:auto;resize:vertical}fieldset{min-width:0;padding:0;margin:0;border:0}legend{display:block;width:100%;max-width:100%;padding:0;margin-bottom:.5rem;font-size:1.5rem;line-height:inherit;color:inherit;white-space:normal}progress{vertical-align:baseline}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto}[type="search"]{outline-offset:-2px;-webkit-appearance:none}[type="search"]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}output{display:inline-block}summary{display:list-item;cursor:pointer}template{display:none}[hidden]{display:none}h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{margin-bottom:.5rem;font-weight:500;line-height:1.2}h1,.h1{font-size:2.5rem}h2,.h2{font-size:2rem}h3,.h3{font-size:1.75rem}h4,.h4{font-size:1.5rem}h5,.h5{font-size:1.25rem}h6,.h6{font-size:1rem}hr{margin-top:1rem;margin-bottom:1rem;border:0;border-top:1px solid rgba(0,0,0,0.1)}.list-unstyled{padding-left:0;list-style:none}.list-inline{padding-left:0;list-style:none}.list-inline-item{display:inline-block}.list-inline-item:not(:last-child){margin-right:.5rem}.img-fluid{max-width:100%;height:auto}.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.header{position:absolute;top:0;left:0;width:100%;z-index:9}amp-selector[role=tablist].tabs-with-flex [role=tab] {flex-grow: 1;/* custom styling, feel free to change */text-align: center;padding: var(--space-1);	background:#ebb240;color:#fff;margin: 5px 20px;border-radius:25px;}.w-100{width:100%;}.form-control {color:#000; padding: 5px 10px; font-family: 'LatoWebMedium';margin-bottom:10px;   font-size: 18px;    font-weight: 400;    border-radius: 5px;    border: none;    letter-spacing: normal;    width: 100%;}.form-control:focus{background-color:#fff;border-color:#ebb240;outline:0;box-shadow:0 0 0 0.2rem rgba(235,178,64,.5);}select.form-control{padding:4px 10px;}
.form-holder{margin-top:20px;}
@font-face{font-family:'Regular_Regular_PRO';src:url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Regular_Regular_PRO.eot');src:url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Regular_Regular_PRO.eot') format('embedded-opentype'),
url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts//Regular_Regular_PRO.woff2') format('woff2'),
url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Regular_Regular_PRO.woff') format('woff'),
url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Regular_Regular_PRO.ttf') format('truetype'),
url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Regular_Regular_PRO.svg#Regular_Regular_PRO') format('svg');}
@font-face {
    font-family: 'LatoWebLight';   src: url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Light.eot'); 
    src: url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Light.eot?#iefix') format('embedded-opentype'),
         url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Light.woff2') format('woff2'), 
         url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Light.woff') format('woff'),  
		 url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Light.ttf') format('truetype');    font-style: normal;    font-weight: normal;    text-rendering: optimizeLegibility;}
	@font-face {    font-family: 'LatoWebMedium';
    src: url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Medium.eot'); 
    src: url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Medium.eot?#iefix') format('embedded-opentype'), 
         url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Medium.woff2') format('woff2'), 
         url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Medium.woff') format('woff'), 
         url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/Lato-Medium.ttf') format('truetype');    font-style: normal;    font-weight: normal;    text-rendering: optimizeLegibility;}
@font-face {    font-family: 'Trajan Pro';
    src: url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/TrajanPro-Regular.eot');    src: url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/TrajanPro-Regular.eot?#iefix') format('embedded-opentype'),
        url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/TrajanPro-Regular.woff2') format('woff2'),   url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts/TrajanPro-Regular.woff') format('woff'),
        url('http://residences.gcorpgroup.com/wp-content/themes/gcorp/fonts//TrajanPro-Regular.ttf') format('truetype');    font-weight: 600;    font-style: normal;}
		.form-bg{ background: rgba(159,135,50,0.9);width:90%;margin:0 auto;}
		.locaul {    padding: 0 50px 0 50px;}
		.aboutxt{padding:15px 10% 0;    margin: 0 auto;}
		#ctn {  position: relative;  height: 100px;}
#ctn a {position: absolute;  left: 0px;  top: -90px;}
#ctn .target-label {  position: absolute;  left: 0px;  top: 0px;  margin: 0;}
.trajanpro{font-family: 'Trajan Pro';}.form-title{width:100%;}.fs-20{font-size:20px}.col-lg-15{width:16%;float:left; padding: 0 10px 10px 0;margin:0 auto;}
.lato-medium{font-family: 'LatoWebMedium';}.lato-light{font-family: 'LatoWebLight';}.ovimg {    float: left;}.ovtxt {    float: right;}
.pb-40 {    padding-bottom: 40px;}.pt-30 {    padding-top: 30px;}
.pro .col-md-4 {    float: left;}.pb-30{padding-bottom:30px;}.bg-black .col-md-4 { float: left;}.bg-black { background: #000; color: #fff;    min-height: 510px;}.bg-black .col-md-8 {   float: left; text-align: center; margin-left: -60px;}
.form-title h4{background: #5b4e22;padding: 10px 0;    text-shadow: 1px 2px 3px #000;    font-family: 'Times New Roman';}.mt-15{margin-top:15px;}.mt-20{margin-top:20px;}.mt-30{margin-top:30px;}.mb-20{margin-bottom:20px;}.btn-primary {border-radius: 35px;    width: 75%;    padding: 0 5px;    display: block;   text-transform: uppercase;
    border: none;    font-family: 'Times New Roman';    font-size: 25px;    font-weight: normal;    text-shadow: 1px 2px 3px #111;    margin: 15px auto;    background-color: #000;    color: #fff;}
	.clr{clear:both;}.color-white{color:#fff;}.fs-16 {    font-size: 16px;    font-weight: 600;}
.g-form { padding: 10px;width:100%;margin:0 auto;}
.form-bg{ background: rgba(159,135,50,0.9);width:90%;margin:0 auto;};
.gform_body input {    border: none;border-radius:10px;
    background: #fff;    padding: 10px 10px!important;
    height: 40px;    color: #000;}p.ph-txt {    display: inline-block;    float: right;}
	.color-gold {    color: #d2ac2a;    text-transform: uppercase;
    font-family: 'Trajan Pro';    font-weight: 400;}.arrw{text-align:center!important;}
	.aboutus .i-amphtml-slide-item>* {
    height: 1000px;    width: 100%;
    overflow: hidden!important;}section.gall {    background: #000;}
.btn.disabled,.btn:disabled {opacity: 1;}.btn-primary.disabled,.btn-primary:disabled {background-color: #ebb240;border-color: #ebb240;}
.project-holder{background: #f7f7f3;position: relative;height: 100%;padding: 0 15px 50px 10px;}.project-holder a {position: relative;z-index: 9;}.project-enquire-btn a{position:absolute;left:0;bottom:0;text-align:center;}
.padding-0{padding:0;}.text-blue{color:#1C7FBE}.min-height-350{min-height:350px;}.min-height-250{min-height:250px;}.col-lg-4 col-md-4 col-sm-12 col-12 padding-0{margin-bottom:20px}footer{background:#d2ac2a;color:#fff;}.fs-12{font-size:12px;}.fixed-footer-btns{position:fixed;bottom:0;left:0;width:100%;z-index:111;color:#fff;}.fixed-footer-btns a{width:100%;float:left;height:40px;line-height:40px;background:#d2ac2a;border-top:1px solid #fff;border-right:1px solid #fff;text-align:center;color:#fff;text-decoration:none;font-size:18px}.fixed-footer-btns a:last-child{border-right:none}.dummy-div{height:50px}
.site-header{width:100%;margin-top:0px;background-color:#fff;height:90px;    position: fixed;    left: 0;    z-index: 9999;}
.site-logo{}.fl-n{float:none;margin:0 auto;}.fs-17{font-size:17px;}
.site-branding a {padding: 0 5px;color: #000;}footer#colophon {    padding: 10px 0;    background: #d2ac2a;    color: #fff;}
.site-branding { margin: 15px 0;}.fs-18 {    font-size: 18px;}.pt-20 {    padding-top: 20px;}
.centabt{    text-align: center;    margin: 0 auto;display: block;}
@media (max-width: 767px){ .pt-xs-30 {    padding-top: 30px;}amp-selector[role=tablist].tabs-with-flex [role=tab][selected] {
        outline: none;        background: transparent;    color: goldenrod!important;border:none!important; }.text-xs-justify{text-align: justify;}.fs-xs-16{font-size:16px;}.text-sm-center {    text-align: center;}.site-header { position: relative;z-index: 0;}.pro {    width: 90%;margin: 0 auto;}
.am-mob {    width: 100px!important;    height: 80px!important;}.pt-xs-20{padding-top:20px;}
.col-lg-15 {    width: 33%;height:145px;}.bg-black .col-md-8 {    margin-left: 0;}.form-banner-caption-holder {    background: rgb(159,135,50);}
.locaul {    padding: 0 20px 0 10px;}.aboutxt {  font-weight:400;  padding: 0 25px!important;    margin: -80px 0 0 0;}
.col-sm-4.ovimg {    float: none;    text-align: center;}
.col-sm-8.ovtxt{   float: none;    width: 100%;    text-align: center;
    padding: 0 0 10px 0;    display: block;}.pb-sm-30{padding-bottom:30px;}
	.ovtxt .row {    margin: 0 auto; width: 100%;    text-align: center;    float: none;    display: block;}}
@media (max-width:576px){.mb-n{    margin-bottom: -75px;}.pb-xs-50{padding-bottom:50px;}}
@media (min-width: 768px) and (max-width: 991px){.fs-sm-16{font-size:16px;}#custom-button-about {margin-top: -110px; clear: both; position: relative;}.col-lg-15 { padding: 10px 0 5px 15px;}.am-mob{width:100px!important;height:80px!important;}section.gall {min-height: 585px;}.pt-sm-10 { padding-top: 10px!important;}.pb-sm-10 { padding-bottom: 10px;}.ovtxt { float: left;  text-align: right;    padding-left: 49px!important;}.fs-sm-15 { font-size: 15px;} .fs-sm-14{ font-size: 14px;}.fs-sm-13{font-size:13px;}.locaul { padding: 0; font-size: 14px;}.bg-black .col-md-8 {    float: none;    text-align: center;    margin: 0 auto;}.pt-sm-0{padding-top:0px}.fs-sm-20{font-size:20px}}
@media (max-width: 576px){.text-xs-justify{text-align:justify!important;}}
@media (min-width: 576px){.container{max-width:540px}}
@media (min-width: 768px){.container{max-width:720px}}
@media (min-width: 992px){.container{max-width:960px}}
@media (min-width: 1200px){.container{max-width:1140px}}.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.no-gutters{margin-right:0;margin-left:0}.no-gutters > .col,.no-gutters > [class*="col-"]{padding-right:0;padding-left:0}.col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-10,.col-11,.col-12,.col,.col-auto,.col-sm-1,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm,.col-sm-auto,.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12,.col-md,.col-md-auto,.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg,.col-lg-auto,.col-xl-1,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl,.col-xl-auto{position:relative;width:100%;padding-right:15px;padding-left:15px}.col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-first{-ms-flex-order:-1;order:-1}.order-last{-ms-flex-order:13;order:13}.order-0{-ms-flex-order:0;order:0}.order-1{-ms-flex-order:1;order:1}.order-2{-ms-flex-order:2;order:2}.order-3{-ms-flex-order:3;order:3}.order-4{-ms-flex-order:4;order:4}.order-5{-ms-flex-order:5;order:5}.order-6{-ms-flex-order:6;order:6}.order-7{-ms-flex-order:7;order:7}.order-8{-ms-flex-order:8;order:8}.order-9{-ms-flex-order:9;order:9}.order-10{-ms-flex-order:10;order:10}.order-11{-ms-flex-order:11;order:11}.order-12{-ms-flex-order:12;order:12}.offset-1{margin-left:8.333333%}.offset-2{margin-left:16.666667%}.offset-3{margin-left:25%}.offset-4{margin-left:33.333333%}.offset-5{margin-left:41.666667%}.offset-6{margin-left:50%}.offset-7{margin-left:58.333333%}.offset-8{margin-left:66.666667%}.offset-9{margin-left:75%}.offset-10{margin-left:83.333333%}.offset-11{margin-left:91.666667%}
@media (min-width: 576px){.col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-sm-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-sm-first{-ms-flex-order:-1;order:-1}.order-sm-last{-ms-flex-order:13;order:13}.order-sm-0{-ms-flex-order:0;order:0}.order-sm-1{-ms-flex-order:1;order:1}.order-sm-2{-ms-flex-order:2;order:2}.order-sm-3{-ms-flex-order:3;order:3}.order-sm-4{-ms-flex-order:4;order:4}.order-sm-5{-ms-flex-order:5;order:5}.order-sm-6{-ms-flex-order:6;order:6}.order-sm-7{-ms-flex-order:7;order:7}.order-sm-8{-ms-flex-order:8;order:8}.order-sm-9{-ms-flex-order:9;order:9}.order-sm-10{-ms-flex-order:10;order:10}.order-sm-11{-ms-flex-order:11;order:11}.order-sm-12{-ms-flex-order:12;order:12}.offset-sm-0{margin-left:0}.offset-sm-1{margin-left:8.333333%}.offset-sm-2{margin-left:16.666667%}.offset-sm-3{margin-left:25%}.offset-sm-4{margin-left:33.333333%}.offset-sm-5{margin-left:41.666667%}.offset-sm-6{margin-left:50%}.offset-sm-7{margin-left:58.333333%}.offset-sm-8{margin-left:66.666667%}.offset-sm-9{margin-left:75%}.offset-sm-10{margin-left:83.333333%}.offset-sm-11{margin-left:91.666667%}.mt-sm-10{margin-top:10px;}}
@media (min-width: 768px){.col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-md-first{-ms-flex-order:-1;order:-1}.order-md-last{-ms-flex-order:13;order:13}.order-md-0{-ms-flex-order:0;order:0}.order-md-1{-ms-flex-order:1;order:1}.order-md-2{-ms-flex-order:2;order:2}.order-md-3{-ms-flex-order:3;order:3}.order-md-4{-ms-flex-order:4;order:4}.order-md-5{-ms-flex-order:5;order:5}.order-md-6{-ms-flex-order:6;order:6}.order-md-7{-ms-flex-order:7;order:7}.order-md-8{-ms-flex-order:8;order:8}.order-md-9{-ms-flex-order:9;order:9}.order-md-10{-ms-flex-order:10;order:10}.order-md-11{-ms-flex-order:11;order:11}.order-md-12{-ms-flex-order:12;order:12}.offset-md-0{margin-left:0}.offset-md-1{margin-left:8.333333%}.offset-md-2{margin-left:16.666667%}.offset-md-3{margin-left:25%}.offset-md-4{margin-left:33.333333%}.offset-md-5{margin-left:41.666667%}.offset-md-6{margin-left:50%}.offset-md-7{margin-left:58.333333%}.offset-md-8{margin-left:66.666667%}.offset-md-9{margin-left:75%}.offset-md-10{margin-left:83.333333%}.offset-md-11{margin-left:91.666667%}.header{position:absolute;top:0;left:0;width:100%;z-index:9}.site-logo,.site-logo-holder{line-height:95px}.banner-caption-form{position:absolute;z-index:99;top:145px;left:0;width:100%}.w-100{width:100%;}#form{border: none;background: rgba(159,135,50,0.9);}
}

@media (min-width: 992px){.col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-lg-first{-ms-flex-order:-1;order:-1}.order-lg-last{-ms-flex-order:13;order:13}.order-lg-0{-ms-flex-order:0;order:0}.order-lg-1{-ms-flex-order:1;order:1}.order-lg-2{-ms-flex-order:2;order:2}.order-lg-3{-ms-flex-order:3;order:3}.order-lg-4{-ms-flex-order:4;order:4}.order-lg-5{-ms-flex-order:5;order:5}.order-lg-6{-ms-flex-order:6;order:6}.order-lg-7{-ms-flex-order:7;order:7}.order-lg-8{-ms-flex-order:8;order:8}.order-lg-9{-ms-flex-order:9;order:9}.order-lg-10{-ms-flex-order:10;order:10}.order-lg-11{-ms-flex-order:11;order:11}.order-lg-12{-ms-flex-order:12;order:12}.offset-lg-0{margin-left:0}.offset-lg-1{margin-left:8.333333%}.offset-lg-2{margin-left:16.666667%}.offset-lg-3{margin-left:25%}.offset-lg-4{margin-left:33.333333%}.offset-lg-5{margin-left:41.666667%}.offset-lg-6{margin-left:50%}.offset-lg-7{margin-left:58.333333%}.offset-lg-8{margin-left:66.666667%}.offset-lg-9{margin-left:75%}.offset-lg-10{margin-left:83.333333%}.offset-lg-11{margin-left:91.666667%}}
@media (min-width: 1200px){.col-xl{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-xl-first{-ms-flex-order:-1;order:-1}.order-xl-last{-ms-flex-order:13;order:13}.order-xl-0{-ms-flex-order:0;order:0}.order-xl-1{-ms-flex-order:1;order:1}.order-xl-2{-ms-flex-order:2;order:2}.order-xl-3{-ms-flex-order:3;order:3}.order-xl-4{-ms-flex-order:4;order:4}.order-xl-5{-ms-flex-order:5;order:5}.order-xl-6{-ms-flex-order:6;order:6}.order-xl-7{-ms-flex-order:7;order:7}.order-xl-8{-ms-flex-order:8;order:8}.order-xl-9{-ms-flex-order:9;order:9}.order-xl-10{-ms-flex-order:10;order:10}.order-xl-11{-ms-flex-order:11;order:11}.order-xl-12{-ms-flex-order:12;order:12}.offset-xl-0{margin-left:0}.offset-xl-1{margin-left:8.333333%}.offset-xl-2{margin-left:16.666667%}.offset-xl-3{margin-left:25%}.offset-xl-4{margin-left:33.333333%}.offset-xl-5{margin-left:41.666667%}.offset-xl-6{margin-left:50%}.offset-xl-7{margin-left:58.333333%}.offset-xl-8{margin-left:66.666667%}.offset-xl-9{margin-left:75%}.offset-xl-10{margin-left:83.333333%}.offset-xl-11{margin-left:91.666667%}}.clearfix::after{display:block;clear:both;content:""}.d-none{display:none}.d-inline{display:inline}.d-inline-block{display:inline-block}.d-block{display:block}.d-table{display:table}.d-table-row{display:table-row}.d-table-cell{display:table-cell}.d-flex{display:-ms-flexbox;display:flex}.d-inline-flex{display:-ms-inline-flexbox;display:inline-flex}
@media (min-width: 576px){.d-sm-none{display:none}.d-sm-inline{display:inline}.d-sm-inline-block{display:inline-block}.d-sm-block{display:block}.d-sm-table{display:table}.d-sm-table-row{display:table-row}.d-sm-table-cell{display:table-cell}.d-sm-flex{display:-ms-flexbox;display:flex}.d-sm-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}
@media (min-width: 768px){.d-md-none{display:none}.d-md-inline{display:inline}.d-md-inline-block{display:inline-block}.d-md-block{display:block}.d-md-table{display:table}.d-md-table-row{display:table-row}.d-md-table-cell{display:table-cell}.d-md-flex{display:-ms-flexbox;display:flex}.d-md-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}
@media (min-width: 992px){.d-lg-none{display:none}.d-lg-inline{display:inline}.d-lg-inline-block{display:inline-block}.d-lg-block{display:block}.d-lg-table{display:table}.d-lg-table-row{display:table-row}.d-lg-table-cell{display:table-cell}.d-lg-flex{display:-ms-flexbox;display:flex}.d-lg-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}
@media (min-width: 1200px){.d-xl-none{display:none}.d-xl-inline{display:inline}.d-xl-inline-block{display:inline-block}.d-xl-block{display:block}.d-xl-table{display:table}.d-xl-table-row{display:table-row}.d-xl-table-cell{display:table-cell}.d-xl-flex{display:-ms-flexbox;display:flex}.d-xl-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}.justify-content-start{-ms-flex-pack:start;justify-content:flex-start}.justify-content-end{-ms-flex-pack:end;justify-content:flex-end}.justify-content-center{-ms-flex-pack:center;justify-content:center}.justify-content-between{-ms-flex-pack:justify;justify-content:space-between}.justify-content-around{-ms-flex-pack:distribute;justify-content:space-around}.align-items-start{-ms-flex-align:start;align-items:flex-start}.align-items-end{-ms-flex-align:end;align-items:flex-end}.align-items-center{-ms-flex-align:center;align-items:center}.align-items-baseline{-ms-flex-align:baseline;align-items:baseline}.align-items-stretch{-ms-flex-align:stretch;align-items:stretch}.align-content-start{-ms-flex-line-pack:start;align-content:flex-start}.align-content-end{-ms-flex-line-pack:end;align-content:flex-end}.align-content-center{-ms-flex-line-pack:center;align-content:center}.align-content-between{-ms-flex-line-pack:justify;align-content:space-between}.align-content-around{-ms-flex-line-pack:distribute;align-content:space-around}.align-content-stretch{-ms-flex-line-pack:stretch;align-content:stretch}.align-self-auto{-ms-flex-item-align:auto;align-self:auto}.align-self-start{-ms-flex-item-align:start;align-self:flex-start}.align-self-end{-ms-flex-item-align:end;align-self:flex-end}.align-self-center{-ms-flex-item-align:center;align-self:center}.align-self-baseline{-ms-flex-item-align:baseline;align-self:baseline}.align-self-stretch{-ms-flex-item-align:stretch;align-self:stretch}@media (min-width: 576px){.justify-content-sm-start{-ms-flex-pack:start;justify-content:flex-start}.justify-content-sm-end{-ms-flex-pack:end;justify-content:flex-end}.justify-content-sm-center{-ms-flex-pack:center;justify-content:center}.justify-content-sm-between{-ms-flex-pack:justify;justify-content:space-between}.justify-content-sm-around{-ms-flex-pack:distribute;justify-content:space-around}.align-items-sm-start{-ms-flex-align:start;align-items:flex-start}.align-items-sm-end{-ms-flex-align:end;align-items:flex-end}.align-items-sm-center{-ms-flex-align:center;align-items:center}.align-items-sm-baseline{-ms-flex-align:baseline;align-items:baseline}.align-items-sm-stretch{-ms-flex-align:stretch;align-items:stretch}.align-content-sm-start{-ms-flex-line-pack:start;align-content:flex-start}.align-content-sm-end{-ms-flex-line-pack:end;align-content:flex-end}.align-content-sm-center{-ms-flex-line-pack:center;align-content:center}.align-content-sm-between{-ms-flex-line-pack:justify;align-content:space-between}.align-content-sm-around{-ms-flex-line-pack:distribute;align-content:space-around}.align-content-sm-stretch{-ms-flex-line-pack:stretch;align-content:stretch}.align-self-sm-auto{-ms-flex-item-align:auto;align-self:auto}.align-self-sm-start{-ms-flex-item-align:start;align-self:flex-start}.align-self-sm-end{-ms-flex-item-align:end;align-self:flex-end}.align-self-sm-center{-ms-flex-item-align:center;align-self:center}.align-self-sm-baseline{-ms-flex-item-align:baseline;align-self:baseline}.align-self-sm-stretch{-ms-flex-item-align:stretch;align-self:stretch}}
@media (min-width: 768px){.justify-content-md-start{-ms-flex-pack:start;justify-content:flex-start}.justify-content-md-end{-ms-flex-pack:end;justify-content:flex-end}.justify-content-md-center{-ms-flex-pack:center;justify-content:center}.justify-content-md-between{-ms-flex-pack:justify;justify-content:space-between}.justify-content-md-around{-ms-flex-pack:distribute;justify-content:space-around}.align-items-md-start{-ms-flex-align:start;align-items:flex-start}.align-items-md-end{-ms-flex-align:end;align-items:flex-end}.align-items-md-center{-ms-flex-align:center;align-items:center}.align-items-md-baseline{-ms-flex-align:baseline;align-items:baseline}.align-items-md-stretch{-ms-flex-align:stretch;align-items:stretch}.align-content-md-start{-ms-flex-line-pack:start;align-content:flex-start}.align-content-md-end{-ms-flex-line-pack:end;align-content:flex-end}.align-content-md-center{-ms-flex-line-pack:center;align-content:center}.align-content-md-between{-ms-flex-line-pack:justify;align-content:space-between}.align-content-md-around{-ms-flex-line-pack:distribute;align-content:space-around}.align-content-md-stretch{-ms-flex-line-pack:stretch;align-content:stretch}.align-self-md-auto{-ms-flex-item-align:auto;align-self:auto}.align-self-md-start{-ms-flex-item-align:start;align-self:flex-start}.align-self-md-end{-ms-flex-item-align:end;align-self:flex-end}.align-self-md-center{-ms-flex-item-align:center;align-self:center}.align-self-md-baseline{-ms-flex-item-align:baseline;align-self:baseline}.align-self-md-stretch{-ms-flex-item-align:stretch;align-self:stretch}}
@media (min-width: 992px){.justify-content-lg-start{-ms-flex-pack:start;justify-content:flex-start}.justify-content-lg-end{-ms-flex-pack:end;justify-content:flex-end}.justify-content-lg-center{-ms-flex-pack:center;justify-content:center}.justify-content-lg-between{-ms-flex-pack:justify;justify-content:space-between}.justify-content-lg-around{-ms-flex-pack:distribute;justify-content:space-around}.align-items-lg-start{-ms-flex-align:start;align-items:flex-start}.align-items-lg-end{-ms-flex-align:end;align-items:flex-end}.align-items-lg-center{-ms-flex-align:center;align-items:center}.align-items-lg-baseline{-ms-flex-align:baseline;align-items:baseline}.align-items-lg-stretch{-ms-flex-align:stretch;align-items:stretch}.align-content-lg-start{-ms-flex-line-pack:start;align-content:flex-start}.align-content-lg-end{-ms-flex-line-pack:end;align-content:flex-end}.align-content-lg-center{-ms-flex-line-pack:center;align-content:center}.align-content-lg-between{-ms-flex-line-pack:justify;align-content:space-between}.align-content-lg-around{-ms-flex-line-pack:distribute;align-content:space-around}.align-content-lg-stretch{-ms-flex-line-pack:stretch;align-content:stretch}.align-self-lg-auto{-ms-flex-item-align:auto;align-self:auto}.align-self-lg-start{-ms-flex-item-align:start;align-self:flex-start}.align-self-lg-end{-ms-flex-item-align:end;align-self:flex-end}.align-self-lg-center{-ms-flex-item-align:center;align-self:center}.align-self-lg-baseline{-ms-flex-item-align:baseline;align-self:baseline}.align-self-lg-stretch{-ms-flex-item-align:stretch;align-self:stretch}}
@media (min-width: 1200px){.justify-content-xl-start{-ms-flex-pack:start;justify-content:flex-start}.justify-content-xl-end{-ms-flex-pack:end;justify-content:flex-end}.justify-content-xl-center{-ms-flex-pack:center;justify-content:center}.justify-content-xl-between{-ms-flex-pack:justify;justify-content:space-between}.justify-content-xl-around{-ms-flex-pack:distribute;justify-content:space-around}.align-items-xl-start{-ms-flex-align:start;align-items:flex-start}.align-items-xl-end{-ms-flex-align:end;align-items:flex-end}.align-items-xl-center{-ms-flex-align:center;align-items:center}.align-items-xl-baseline{-ms-flex-align:baseline;align-items:baseline}.align-items-xl-stretch{-ms-flex-align:stretch;align-items:stretch}.align-content-xl-start{-ms-flex-line-pack:start;align-content:flex-start}.align-content-xl-end{-ms-flex-line-pack:end;align-content:flex-end}.align-content-xl-center{-ms-flex-line-pack:center;align-content:center}.align-content-xl-between{-ms-flex-line-pack:justify;align-content:space-between}.align-content-xl-around{-ms-flex-line-pack:distribute;align-content:space-around}.align-content-xl-stretch{-ms-flex-line-pack:stretch;align-content:stretch}.align-self-xl-auto{-ms-flex-item-align:auto;align-self:auto}.align-self-xl-start{-ms-flex-item-align:start;align-self:flex-start}.align-self-xl-end{-ms-flex-item-align:end;align-self:flex-end}.align-self-xl-center{-ms-flex-item-align:center;align-self:center}.align-self-xl-baseline{-ms-flex-item-align:baseline;align-self:baseline}.align-self-xl-stretch{-ms-flex-item-align:stretch;align-self:stretch}}.float-left{float:left}.float-right{float:right}.float-none{float:none}
@media (min-width: 576px){.float-sm-left{float:left}.float-sm-right{float:right}.float-sm-none{float:none}amp-selector[role=tablist].tabs-with-flex [role=tab] {flex-grow: 1;/* custom styling, feel free to change */text-align: center;padding: var(--space-1);	background:#ebb240;color:#fff;margin: 5px 20px;border-radius:25px;}}
@media (min-width: 768px){.bg-black amp-img {    float: right;}#custom-button-banner .amp-carousel-button.amp-carousel-button-prev { opacity: 0;}#custom-button-banner .amp-carousel-button.amp-carousel-button-next {    opacity: 0;}.float-md-left{float:left}.float-md-right{float:right}.float-md-none{float:none}}
@media (min-width: 992px){.float-lg-left{float:left}.float-lg-right{float:right}.float-lg-none{float:none}}
@media(min-width:1200px){.float-xl-left{float:left}.float-xl-right{float:right}.float-xl-none{float:none}}.overflow-auto{overflow:auto}.overflow-hidden{overflow:hidden}.position-static{position:static}.position-relative{position:relative}.position-absolute{position:absolute}.position-fixed{position:fixed}.position-sticky{position:-webkit-sticky;position:sticky}.fixed-top{position:fixed;top:0;right:0;left:0;z-index:1030}.fixed-bottom{position:fixed;right:0;bottom:0;left:0;z-index:1030}.w-25{width:25%}.w-50{width:50%}.w-75{width:75%}.w-100{width:100%}.w-auto{width:auto}.h-25{height:25%}.h-50{height:50%}.h-75{height:75%}.h-100{height:100%}.h-auto{height:auto}.mw-100{max-width:100%}.mh-100{max-height:100%}.min-vw-100{min-width:100vw}.min-vh-100{min-height:100vh}.vw-100{width:100vw}.vh-100{height:100vh}.text-monospace{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}.text-justify{text-align:justify}.text-wrap{white-space:normal}.text-nowrap{white-space:nowrap}.text-truncate{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}.text-left{text-align:left}.text-right{text-align:right}.text-center{text-align:center}
@media (min-width: 576px){.text-sm-left{text-align:left}.text-sm-right{text-align:right}.text-sm-center{text-align:center}}
@media (min-width: 768px){.text-md-left{text-align:left}.text-md-right{text-align:right}.text-md-center{text-align:center}}
@media (min-width: 992px){.text-lg-left{text-align:left}.text-lg-right{text-align:right}.text-lg-center{text-align:center}}
@media(min-width:1200px){.text-xl-left{text-align:left}.text-xl-right{text-align:right}.text-xl-center{text-align:center}}.text-lowercase{text-transform:lowercase}.text-uppercase{text-transform:uppercase}.text-capitalize{text-transform:capitalize}.font-weight-light{font-weight:300}.font-weight-lighter{font-weight:lighter}.font-weight-normal{font-weight:400}.font-weight-bold{font-weight:700}.font-weight-bolder{font-weight:bolder}.font-italic{font-style:italic}.text-white{color:#fff}.text-blue{color:#1C7FBE}
:root {
      --color-primary: #005AF0;
      --space-1: .5rem;  /* 8px */
      --space-4: 2rem;   /* 32px */ }
       amp-selector[role=tablist].tabs-with-flex {
        display: flex; flex-wrap: wrap;    }
    amp-selector[role=tablist].tabs-with-flex [role=tab] {
        flex-grow: 1; text-align: center;        padding: var(--space-1);		background:transparent;
		color:#fff;		margin: 5px 20px;border-radius:0;    }
    amp-selector[role=tablist].tabs-with-flex [role=tab][selected] {
        outline: none;        background: transparent;    color: goldenrod;	border-bottom:2px solid #fff;    }
    amp-selector[role=tablist].tabs-with-flex [role=tabpanel] {
        display: none;        width: 100%;        order: 1;     padding: 0;    }
    amp-selector[role=tablist].tabs-with-flex [role=tab][selected] + [role=tabpanel] {
        display: block;    }
    amp-selector[role=tablist].tabs-with-selector {        display: flex;    }
    amp-selector[role=tablist].tabs-with-selector [role=tab][selected] {
        outline: none  border-bottom: 2px solid var(--color-primary);
		background-color: #1C80BE; color: #fff;    }
    amp-selector[role=tablist].tabs-with-selector {        display: flex;    }
    amp-selector[role=tablist].tabs-with-selector [role=tab] { width: 100%;
      text-align: center; padding: var(--space-1);    }
    amp-selector.tabpanels [role=tabpanel] { display: none; padding: var(--space-4);    }
    amp-selector.tabpanels [role=tabpanel][selected] {
		background-color: #1C80BE;		color: #fff;      outline: none;      display: block;    }
amp-selector[role=tablist] [role=tab][selected] + [role=tabpanel] {  display: block;}
</style>
</head>
<body>
<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-MPQHGB8&gtm.url=<?php echo $actual_link; ?>" data-credentials="include"></amp-analytics>
<header id="masthead" class="site-header">
		<div class="container">
			<div class="row">
				<div class="site-branding col-lg-2 col-md-3 col-sm-12 col-xs-12 text-xs-center text-sm-center text-lg-left">
					<amp-img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" width="160" height="60" class="site-logo" alt="logo" /></amp-img>
				</div>
				<div class="site-branding col-lg-7 col-md-6 col-sm-12 col-xs-12 text-xs-center text-sm-center text-center fs-18 fs-sm-15 pt-20 pt-sm-10 pt-xs-0">
					<a href="#pro" class="scroll">Project Overview</a> | <a href="#loca" class="scroll">Location</a> | <a href="#ament" class="scroll">Amenities</a> | <a href="#about" class="scroll">About Us</a>
				</div>
				<div class="site-branding col-lg-3 col-md-3 col-sm-12 col-xs-12 fs-18 fs-sm-14  pt-20 pt-sm-10 text-right">
					<div class="col-12 pt-sm-0"><amp-img class="ph-mob" src="<?php echo get_template_directory_uri(); ?>/images/phone.png" width="28" height="24" alt="" /></amp-img><p class="ph-txt">7676131888</p></div>


				</div>
			</div>
		</div>
	</header>
	<section class="banner pt-0">
	<amp-carousel id="custom-button-banner"  class="d-none d-sm-none d-md-block d-lg-block w-100" width="1366" height="650" layout="responsive" type="slides" autoplay delay="3000">
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/banner.png" width="1366" height="650" layout="responsive" alt="a sample image"></amp-img>
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/bannerone.png" width="1366" height="650" layout="responsive" alt="a sample image"></amp-img>
		
	</amp-carousel>
	<amp-carousel class="d-block d-sm-block d-md-none d-lg-none w-100" width="480" height="768" layout="responsive" type="slides" autoplay delay="3000">
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/banner-mob.png" width="480" height="768" layout="responsive" class="mobile-image w-100" alt="and another sample image"></amp-img>
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/bannerone-mob.png" width="480" height="768" layout="responsive" class="mobile-image w-100" alt="and another sample image"></amp-img>
	</amp-carousel>
	<div class="banner-caption-form">
		<div class="container">
			<div class="row form-banner-caption-holder">
				<div class="col-12 col-sm-12 col-lg-9 col-md-7 mt-60 mt-xs-10 mt-sm-10 "></div>
				<div class="col-12 col-sm-12 col-lg-3 col-md-4 form-holder mt-xs-10 mt-sm-10 " id="form">
					<div class="row">
						<div class="form-title">
							<h4 class="text-center color-white regular-pro" style="font-weight:normal;">CONTACT US</h4>
						</div>
					</div>
					<div class="g-form">
					<form class="desktop-form padd-both-15 pb-15 mt-15 mt-xs-10 mt-sm-10" action="<?php echo get_site_url(); ?>/gravity.php" target="_top" method="GET" role="form" data-toggle="validator">
						<div class="form-group">
							<input type="text" name="Name" id="show-all-on-submit-name" class="form-control" placeholder="Name *" pattern="^[a-zA-Z]+( [a-zA-Z]+)*$" data-error="Please enter your name" required />
							<span visible-when-invalid="valueMissing" validation-for="show-all-on-submit-name"> Please enter your name!</span>
							<span visible-when-invalid="patternMismatch" validation-for="show-all-on-submit-name"> Please enter valid name!</span>
						</div>
						<div class="form-group">
							<input type="text" name="Phone" pattern="^\d{10}$" id="show-all-on-submit-Mobile"  class="form-control" placeholder="Phone *" data-error="Valid number please" required />
							<span visible-when-invalid="valueMissing" validation-for="show-all-on-submit-Mobile"> Please enter your Mobile!</span>
						<span visible-when-invalid="patternMismatch" validation-for="show-all-on-submit-Mobile"> Please enter valid Mobile!</span>	
						</div>
						<div class="form-group">
							<input type="email" name="Email" id="show-all-on-submit-email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email *" data-error="Vaild email please" required />
							<span visible-when-invalid="valueMissing" validation-for="show-all-on-submit-email"> Please enter your email!</span>
						<span visible-when-invalid="patternMismatch" validation-for="show-all-on-submit-email"> Please enter valid email!</span>
						</div>
						
						<div class="form-group">
							<select class="form-control" name="Budget" data-error="Please select a Budget" required />
								<option value="">Purchase Budget</option>
								<option value="1 Cr. – 2 Cr.">1 Cr. – 2 Cr.</option>
								<option value="2 Cr. – 4 Cr.">2 Cr. – 4 Cr.</option>
								
							</select>
						</div>
						<div class="form-group mb-0 text-center">
							<button type="submit" class="btn btn-primary w-100 text-uppercase">SUBMIT</button>
						</div>
						<input type="hidden" class="form-url"  name="SourceURL" value="<?php echo $actual_link?>" />
					</form> 
					</div>
				</div> 
			</div>
		</div>
	</div>
</section>

<section class="pro" id="project">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 pt-30 pb-0">
			<div id="ctn">
  <a name="pro">&nbsp;</a>
				<h2 class="fs-30 color-gold pt-30 pb-30 text-center">Project Overview</h2>
				</div>
				<div class="text-justify lato-medium fs-20 fs-xs-16 fs-sm-16 pb-40 pt-xs-30 pb-xs-10">
					<?php the_field('main-content');?>
				</div>
				<div class="col-sm-12">
					<div class="col-md-4">
						<div class="col-sm-4 ovimg"><amp-img src="<?php echo get_template_directory_uri(); ?>/images/overview1.png" width="83" height="77" alt="Overview"></amp-img></div>
						<div class="col-sm-8 fs-sm-14 ovtxt pt-30 pt-sm-0">
							<div class="row">2, 3 & 4 BHK <br> Residential Apartments</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg"><amp-img src="<?php echo get_template_directory_uri(); ?>/images/overview2.png" width="83" height="77" alt="Overview"></amp-img></div>
						<div class="col-sm-8 fs-sm-14 ovtxt pt-30 pt-sm-0">
							<div class="row">234 Units <br>(954 sq ft - 2197 sq ft)</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg"><amp-img src="<?php echo get_template_directory_uri(); ?>/images/overview3.png" width="83" height="77" alt="Overview"></amp-img></div>
						<div class="col-sm-8 fs-sm-14 ovtxt pt-30 pt-sm-0">
							<div class="row">Located In The <br> Heart Of Koramangala</div>
						</div>
					</div>
					<div class="clr"></div>
					<div class="pb-30"></div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg"><amp-img src="<?php echo get_template_directory_uri(); ?>/images/overview4.png" width="83" height="77"  alt="Overview"></amp-img></div>
						<div class="col-sm-8 fs-sm-14 ovtxt pt-30 pt-sm-0">
							<div class="row">Possession By <br> 2021</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg"><amp-img src="<?php echo get_template_directory_uri(); ?>/images/overview5.png" width="83" height="77"  alt="Overview"></amp-img></div>
						<div class="col-sm-8 fs-sm-14 ovtxt pt-30 pt-sm-0">
							<div class="row">IGBC Pre-Certified <br> For Gold Rating </div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-sm-4 ovimg"><amp-img src="<?php echo get_template_directory_uri(); ?>/images/overview6.png" width="83" height="77" alt="Overview"></amp-img></div>
						<div class="col-sm-8 fs-sm-14 ovtxt pt-sm-0 pt-30">
							<div class="row">20+ <br> Amenities</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="location" id="location">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 pt-xs-20">
			          <div id="ctn" class="d-none d-xs-none d-sm-none d-md-block d-lg-block">
                               <a name="loca">&nbsp;</a>
						         <h2 class="fs-13 color-gold pb-0 text-center">&nbsp;</h2>
						    </div>
			<div class="col-sm-12 d-block d-xs-block d-sm-block d-md-none d-lg-none">
					<a href="https://www.google.com/maps/place/G+Corp+Residences/@12.9303569,77.6344086,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae1466dd6e22ef:0xd70dcbebfc4472ca!8m2!3d12.9303569!4d77.6365973" target="_blank"><amp-img src="<?php echo get_template_directory_uri(); ?>/images/map-mob.jpg" width="350" height="400" alt="" layout="responsive" class="imgloca mb-20"></amp-img></a>
					</div>
				<div class="row">
					<div class="col-sm-8">
						<div class="row d-none d-xs-none d-sm-none d-md-block d-lg-block">
							<a href="https://www.google.com/maps/place/G+Corp+Residences/@12.9303569,77.6344086,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae1466dd6e22ef:0xd70dcbebfc4472ca!8m2!3d12.9303569!4d77.6365973" target="_blank"><amp-img src="<?php echo get_template_directory_uri(); ?>/images/locationimg.png" width="1011" height="510" alt="" class="imgloca" /></amp-img></a></div>
					</div>
										
					
					<div class="col-sm-4">
						<div class="row">
							<div class="bg-black pt-30 pb-30 pb-sm-10">
								<?php the_field('location-text'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="amen" id="amenities">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-sm-12">
				         	<div id="ctn">
                               <a name="ament">&nbsp;</a>
						         <h2 class="fs-30 color-gold pt-30 pb-10 text-center">Amenities</h2>
						    </div>
						<h5 class="fs-18 pt-20 text-center pb-10">AT GROUND LEVEL</h5>
						<div class="col-sm-12">
							<?php
				                $i=0;
	                          if( have_rows('amenties-ground') ):
	                        while ( have_rows('amenties-ground') ) : the_row();
	                            	if($i % 6 == 0){ ?>
							<div class="col-lg-15 pb-20 pb-xs-0">
								<?php }else{  ?>
								<div class="col-lg-15 pb-20 pb-xs-0">
									<?php }?>
									<amp-img class="pb-xs-20 text-center am-mob" width="155" height="120" src="<?php the_sub_field('amenities-img1'); ?>"></amp-img>
									<h5 class="fs-16 text-center pt-10">
										<?php the_sub_field('amenities-txt1'); ?>
									</h5>
								</div>
								<?php $i++; endwhile;
						else :
		            endif;
			      	?>
							</div>
						</div>
						<div class="clr"></div>
						<h5 class="fs-18 pt-30 text-center fl-n pb-10">AT 8TH LEVEL</h5>
						<div class="col-sm-12">
							<?php
				$i=0;
	            if( have_rows('amenties-8level') ):
	                while ( have_rows('amenties-8level') ) : the_row();
	                	if($i % 6 == 0){ ?>
							<div class="col-lg-15 pb-20 pb-xs-0">
								<?php }else{  ?>
								<div class="col-lg-15 pb-20 pb-xs-0">
									<?php }?>
									<amp-img class="pb-xs-20 text-center am-mob" width="155" height="120" src="<?php the_sub_field('amenities-img2'); ?>"></amp-img>
									<h5 class="fs-16 text-center pt-10">
										<?php the_sub_field('amenities-txt2'); ?>
									</h5>
								</div>
								<?php $i++; endwhile;
						else :
		            endif;
			      	?>
							</div>
						</div>
						<div class="clr"></div>
						<h5 class="fs-18 pt-30 text-center pb-10">AT 14TH LEVEL</h5>
						<div class="col-sm-12 mb-30">
							<?php
				$i=0;
	            if( have_rows('amenties-14level') ):
	                while ( have_rows('amenties-14level') ) : the_row();
	                	if($i % 6 == 0){ ?>
							<div class="offset-1 col-lg-15 pb-40 pb-xs-0">
								<?php }else{  ?>
								<div class="col-lg-15 pb-40 pb-xs-0">
									<?php }?>
									<amp-img class="pb-xs-20 text-center am-mob" width="155" height="120" src="<?php the_sub_field('amenities-img'); ?>"></amp-img>
									<h5 class="fs-16 text-center pt-10">
										<?php the_sub_field('amenities-txt'); ?>
									</h5>
								</div>
								<?php $i++; endwhile;
						else :
		            endif;
			      	?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>

</section>
<section class="gall">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			
				<h2 class="fs-30 color-gold pt-30 pb-0 text-center">Gallery</h2>
			</div>
		  <div class="col-lg-12 col-md-12 col-sm-12 text-center ml-auto mr-auto pb-sm-30" id="gallery-actions">
				<amp-selector class="mt-30 mt-xs-10 mt-sm-10 tabs-with-flex" role="tablist">
				
					<div id="tab1" role="tab" aria-controls="tabpanel1" option selected>Floorplans</div>
					
					
				
						<div id="tabpanel1" role="tabpanel" aria-labelledby="tab1" class="pt-sm-10">
						<amp-carousel id="custom-button" class="d-none d-sm-none d-md-block d-lg-block w-100" width="1100" height="650" layout="responsive" type="slides" autoplay delay="3000">					  
	                          <amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor11-1.png" width="600" height="500" alt=""></amp-img>
							<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor12-1.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor13.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor14.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor15.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor16.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor17.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor18.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor19.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor110.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor111.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor112.png" width="600" height="500" alt=""></amp-img>									
						</amp-carousel>
						<amp-carousel id="custom-button1" class="d-block d-sm-block d-md-none d-lg-none w-100" width="480" height="500" layout="responsive" type="slides" autoplay delay="3000">					  
	                          <amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor11-1.png" layout="responsive" width="420" height="400" alt=""></amp-img>
							<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor12-1.png" layout="responsive" width="420" height="400" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor13.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor14.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor15.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor16.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor17.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor18.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor19.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor110.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor111.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/floor112.png" width="600" height="500" alt=""></amp-img>									
						</amp-carousel>
							</div>
							<div id="tab2" role="tab" aria-controls="tabpanel2" option>Project Images</div>
						<div id="tabpanel2" role="tabpanel" aria-labelledby="tab2" class="pt-sm-10">
						<amp-carousel id="custom-button" class="d-none d-sm-none d-md-block d-lg-block w-100" width="1100" height="650" layout="responsive" type="slides" autoplay delay="3000">					  
	                          <amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/build003.png" width="600" height="500" alt=""></amp-img>
							<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/build001.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/build002.png" width="600" height="500" alt=""></amp-img>	
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/build004.png" width="600" height="500" alt=""></amp-img>								
						</amp-carousel>
						<amp-carousel id="custom-button2" class="d-block d-sm-block d-md-none d-lg-none w-100" width="480" height="500" layout="responsive" type="slides" autoplay delay="3000">					  
	                          <amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/build003.png" layout="responsive" width="420" height="400" alt=""></amp-img>
							<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/build001.png" layout="responsive" width="420" height="400" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/build002.png" layout="responsive" width="420" height="400" alt=""></amp-img>	
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/build004.png" layout="responsive" width="420" height="400" alt=""></amp-img>								
						</amp-carousel>
							</div>
							<div id="tab3" role="tab" aria-controls="tabpanel3" option>Amenities</div>
						<div id="tabpanel3" role="tabpanel" aria-labelledby="tab3" class="pt-sm-10">
						<amp-carousel id="custom-button" class="d-none d-sm-none d-md-block d-lg-block w-100" width="1100" height="650" layout="responsive" type="slides" autoplay delay="3000">					  
	                          <amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/amenities01.png" width="600" height="500" alt=""></amp-img>
							<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/amenities02.png" width="600" height="500" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/amenities03.png" width="600" height="500" alt=""></amp-img>								
						</amp-carousel>
						<amp-carousel id="custom-button3" class="d-block d-sm-block d-md-none d-lg-none w-100" width="480" height="500" layout="responsive" type="slides" autoplay delay="3000">					  
	                          <amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/amenities01.png" layout="responsive" width="420" height="400" alt=""></amp-img>
							<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/amenities02.png" layout="responsive" width="420" height="400" alt=""></amp-img>
<amp-img src="http://residences.gcorpgroup.com/wp-content/uploads/2019/03/amenities03.png" layout="responsive" width="420" height="400" alt=""></amp-img>								
						</amp-carousel>
							</div>
						</amp-selector>	
		         </div>
		</div>
</div>
</section>
<section class="aboutus" id="aboutus">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-sm-12">
						<div class="row">
							<div class="col-sm-12 d-block d-xs-block d-sm-block d-md-none d-lg-none">
								<div class="col-sm-12 mb-n">
									<h2 class="fs-28 color-gold pt-30 pb-0 text-center">ABOUT US</h2>
									<amp-img src="<?php echo get_template_directory_uri(); ?>/images/aboutG.png" width="187" height="70" class="centabt text-center" alt=""></amp-img>
								</div>
							</div>
							<amp-carousel id="custom-button-about"  class="d-none d-sm-none d-md-block d-lg-block w-100" width="1366" height="650" layout="fixed" type="slides" autoplay delay="3000">
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/abgal3.png" width="1000" height="1000" layout="responsive" alt="a sample image"></amp-img>
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/abgal2.png" width="1000" height="1000" layout="responsive" alt="a sample image"></amp-img>
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/abgal1.png" width="1000" height="1000" layout="responsive" alt="a sample image"></amp-img>
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/abgal4.png" width="1000" height="1000" layout="responsive" alt="a sample image"></amp-img>
		
	</amp-carousel>
	<amp-carousel class="d-block d-sm-block d-md-none d-lg-none w-100" width="480" height="768" layout="responsive" type="slides" autoplay delay="3000">
        <amp-img src="<?php echo get_template_directory_uri(); ?>/images/abgal3.png" width="480" height="400" layout="responsive" class="mobile-image w-100" alt="a sample image"></amp-img>
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/abgal2.png" width="480" height="400" layout="responsive" class="mobile-image w-100" alt="a sample image"></amp-img>
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/abgal1.png" width="480" height="400" layout="responsive" class="mobile-image w-100" alt="a sample image"></amp-img>
		<amp-img src="<?php echo get_template_directory_uri(); ?>/images/abgal4.png" width="480" height="400" layout="responsive" class="mobile-image w-100" alt="a sample image"></amp-img>	
  </amp-carousel>
						
						</div>
					</div>
					<div class="col-lg-5 col-md-5">
						<div class="row">
							<div class="col-sm-12 d-none d-xs-none d-sm-none d-md-block d-lg-block">
								<div class="col-sm-12">
								<div id="ctn">
                            <a name="about">&nbsp;</a>
									<h2 class="fs-28 color-gold pt-30 pt-sm-10 pb-0 text-center">ABOUT US</h2>
								</div>
								<amp-img src="<?php echo get_template_directory_uri(); ?>/images/aboutG.png" class="centabt text-center" width="187" height="70" alt=""></amp-img>						
							     </div>
							</div>
								 <div class="col-sm-12 col-md-10 col-lg-10 fs-16 fs-sm-13 aboutxt text-center pt-10 pt-xs-30">
								       <?php the_field('about-text'); ?>
							       </div>
						      </div>
					      </div>
				        </div>
			        </div>
		       </div>
            </div>
</section>
<footer id="colophon" class="site-footer">
		<div class="site-info">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<p class="fs-17 mt-0 mb-0 mb-xs-20 pb-xs-50">RERA Approved- PRKN/170729/000249<br>Copyright &copy; 2019, G:Corp Group All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<div class="fixed-footer-btns d-block d-xs-block d-sm-block d-md-none d-lg-none"><a class="scroll" href="#form">Enquire Now</a></div>

</body>
</html>
