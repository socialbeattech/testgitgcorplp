<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package xseed
 */

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<p class="fs-17 mt-0 mb-0 mb-xs-20 pb-xs-50">RERA Approved- PRKN/170729/000249<br>Copyright &copy; 2019, G:Corp Group All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	<?php if(is_front_page()) { ?>
	<div class="fixed-footer-btns d-block d-xs-block d-sm-block d-md-none d-lg-none"><a class="scroll" href="#form">Enquire Now</a></div>
	<?php } else { ?>
	<?php } ?>
<?php wp_footer(); ?>

<script>
var mySwiper = new Swiper('.swiper-container', {
  pagination: '.swiper-pagination',
  effect: 'coverflow',
  grabCursor: true,
  centeredSlides: true,
  slidesPerView: 2,
  coverflow: {
    rotate: 50,
    stretch: 0,
    depth: 100,
    modifier: 1,
    slideShadows : true
  },
  loop: true,
	navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
	breakpoints: {
	  1023: {
	    slidesPerView: 1,
	    spaceBetween: 0
	  }
	}
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
  var paneTarget = $(e.target).attr('href');
  var $thePane = $('.tab-pane' + paneTarget);
  var paneIndex = $thePane.index();
  if ($thePane.find('.swiper-container').length > 0 && 0 === $thePane.find('.swiper-slide-active').length) {
    mySwiper[paneIndex].update();
  }
});
</script>


</body>
</html>
