<?php
require('wp-config.php');

//Validating the form
$siteUrl = $_GET['SourceURL'];
$mobileValid = false;
$EmailValid = false;
function validate_mobile($mobile) {
	return preg_match('/^[0-9]{10}+$/', $mobile);
}
function isValidEmail($Email) {
	return filter_var($Email, FILTER_VALIDATE_EMAIL) !== false;
}
if (validate_mobile($_GET['Phone'])) {
	$mobileValid = true;
} else {
	echo "<h1>Mobile Number is of incorrect format</h1><br><p>Mobile number should contain only 10 digits.</p>";
	header("refresh:2;url={$siteUrl}");

}
if (isValidEmail($_GET['Email'])) {
	$EmailValid = true;
} else {
	echo "Email Address given is Invalid";
	header("refresh:2;url={$siteUrl}");
}

if (isset($_GET['Name']) && $mobileValid == true && $EmailValid == true) {
	$firstthreechar = substr($_GET['Name'], 0, 2);
	if (1 === preg_match('~[0-9]~', $_GET['Name'])) {
		echo 'Spam';
	} else {
		
//API for gravity form
$api_key = 'afeeb77faf';
$private_key = '43522b3b324c172';
 
//set route
$route = 'forms/2/entries';

function calculate_signature( $string, $private_key ) {
    $hash = hash_hmac( 'sha1', $string, $private_key, true );
    $sig = rawurlencode( base64_encode( $hash ) );
    return $sig;
}


//creating request URL
$expires = strtotime( '+60 mins' );
$string_to_sign = sprintf( '%s:%s:%s:%s', $api_key, 'POST', $route, $expires );
$sig = calculate_signature( $string_to_sign, $private_key );
$url = get_site_url() . '/gravityformsapi/' . $route . '?api_key=' . $api_key . '&signature=' . $sig . '&expires=' . $expires;

 $phone = $_GET['Phone'];

$entries = array(
    array(
        'date_created' => date('Y-m-d H:i:s'),
        'is_starred'   => 0,
        'is_read'      => 0,
        'ip'           => '::1',
        'source_url'   => $_GET['SourceURL'],
        'currency'     => 'USD',
        'created_by'   => 1,
        'user_agent'   => 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
        'status'       => 'active',
        '1'            => $_GET['Name'],
        '2'            => $_GET['Email'],
        '3'            => $phone,
        '8'            => $_GET['Budget'],
		'5'            => $_GET['SourceURL'],
    )
);
 //json encode array
  $entry_json = json_encode( $entries );

  //retrieve data
  $response = wp_remote_request( $url , array( 'method' => 'POST', 'sslverify' => false, 'body' => $entry_json, 'timeout' => 25 ) );
  $res = wp_remote_retrieve_body( $response );
  $res1 = wp_remote_retrieve_response_code( $response );
  if ( $res1 != 200 || ( empty($res) ) ){
      //http request failed
      die( 'There was an error attempting to access the API.' );
  }

  //result is in the response "body" and is json encoded.
  $body = json_decode( wp_remote_retrieve_body( $response ), true );

  if( $body['status'] > 202 ){
      $error = $body['response'];

          //entry update failed, get error information, error could just be a string
      if ( is_array( $error )){
          $error_code     = $error['code'];
          $error_message  = $error['message'];
          $error_data     = isset( $error['data'] ) ? $error['data'] : '';
          $status     = "Code: {$error_code}. Message: {$error_message}. Data: {$error_data}.";
      }
      else{
          $status = $error;
      }
      die( "Could not post entries. {$status}" );
  } else {

  	$url = $_GET['site-url'].'/thanks-amp/';
  	header('Location: '.$url);  }
	
  }
} elseif (empty($_GET['Name'])) {
	echo "<h1>Name is missing</h1>";
	echo "Redirecting......";
	header("refresh:2;url={$siteUrl}");
} elseif (empty($_GET['mobile'])) {
	echo "<h1>Mobile Number is missing</h1>";
	echo "Redirecting......";
	header("refresh:2;url={$siteUrl}");
} elseif (empty($_GET['Email'])) {
	echo "<h1>Email is missing</h1>";
	echo "Redirecting......";
	header("refresh:2;url={$siteUrl}");
} elseif (empty($_GET['Name']) && empty($_GET['mobile']) && empty($_GET['Email'])) {
	echo "All Fields are required";
	header("refresh:2;url={$siteUrl}");
}	
?>